# Server partition table

```
NAME                SIZE  TYPE  MOUNTPOINT
sda                  40G  disk  
├─sda1                1G  part  /boot
├─sda2               10G  crypt /
└─sda3               29G
  └─vg1-docker       29G  lvm   /var/lib/docker
sdb                  10G  disk  
└─backup             10G  crypt 
  ├─vg0-securedata  100M  lvm   /mnt/securedata
  └─vg0-backup      9.9G  lvm   /mnt/backup
sdc                  xxG  disk                     # Possible disk extension
└─data               xxG  crypt
  └─vg1-docker       xxG  lvm   /var/lib/docker
```
